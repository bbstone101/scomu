//package com.bbstone.comm.demo;
//
//import lombok.extern.slf4j.Slf4j;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Slf4j
//public class LocalCacheTest {
//
//
//    public static void main(String[] args) {
//        Map<String, String> map = new HashMap<>();
//        for (int i = 0; i < 5; i++) {
//            map.put("Key-" + i, "value=" + i);
//        }
//        LocalCache.putAll(map);
//
//        log.info("estimatedSize: {}", LocalCache.estimatedSize());
//        LocalCache.invalidate("key-3");
//        log.info("estimatedSize: {}", LocalCache.estimatedSize());
//        Map<String, String> resultMap = LocalCache.getAll(map.keySet(), keys -> {
//            keys.iterator().forEachRemaining(key -> System.out.println(key));
//            return map;});
//        resultMap.keySet().iterator().forEachRemaining(val -> {System.out.println(val);});
//
//
//    }
//}
