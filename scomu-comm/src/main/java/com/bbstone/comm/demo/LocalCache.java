//package com.bbstone.comm.demo;
//
//import com.github.benmanes.caffeine.cache.Cache;
//import com.github.benmanes.caffeine.cache.Caffeine;
//import com.github.benmanes.caffeine.cache.RemovalCause;
//import com.github.benmanes.caffeine.cache.stats.CacheStats;
//import org.checkerframework.checker.units.qual.K;
//
//import java.util.Iterator;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.TimeUnit;
//import java.util.function.Function;
//
//public class LocalCache {
//
//    private static Cache<String, String> caches = Caffeine.newBuilder().maximumSize(100_000)
//            .expireAfterWrite(10, TimeUnit.MINUTES)
//            .recordStats()
//            .removalListener((String key, String value, RemovalCause cause) ->{
//                System.out.printf("移除缓存Key:%s  Value：%s 移除类型：%s %n", key, value ,cause);
//            })
//            .build();
//
//    public static void put(String key, String value) {
//        caches.put(key, value);
//    }
//
//    public static void putAll(Map<String, String> items) {
//        caches.putAll(items);
//    }
//
//    public static String get(String key) {
//        return caches.getIfPresent(key);
//    }
//
//    public static Map<String, String> getAllPresent(Iterable<String> keys) {
//        return caches.getAllPresent(keys);
//    }
//
//    // , Function<? super Set<? extends String>, ? extends Map<? extends String, ? extends String>> func
//    public static Map<String, String> getAll(Iterable<String> iterator, Function<? super Set<? extends String>, ? extends Map<? extends String, ? extends String>> func) {
//        return caches.getAll(iterator, func);
//    }
//
//    public static void cleanUp() {
//        caches.cleanUp();
//    }
//
//    public static void invalidate(String key) {
//        caches.invalidate(key);
//    }
//
//    public static void invalidateAll() {
//        caches.invalidateAll();
//    }
//
//    public static void invalidateAll(Iterable<String> keys) {
//        caches.invalidateAll(keys);
//    }
//
//    public static long estimatedSize() {
//        return caches.estimatedSize();
//    }
//
//    public static CacheStats stats() {
//        return caches.stats();
//    }
//
//}
