package com.bbstone.client.core;

import java.util.ArrayList;
import java.util.List;

import com.bbstone.client.core.ext.AuthSuccessListener;
import com.bbstone.client.core.model.ConnStatus;
import com.bbstone.client.core.model.Scheduler;
import com.bbstone.comm.model.ConnInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * Client Connection Manager
 * 
 *
 * @author bbstone
 *
 */
@Slf4j
public class ClientConnectionManager {
	
//	public static ClientContext open(ConnInfo connInfo) {
//		log.info("open new connection. connId: {}", connInfo.connId());
//		ClientConnector clientConnector = ClientContextHolder.initConnector(connInfo.connId());
//		// new & save context
//		ClientContext clientContext = ClientContextHolder.newContext(connInfo);
//		// connect server
//		clientConnector.connect(clientContext);
//
//		return clientContext;
//	}
//
//	public static ClientContext[] open(ConnInfo[] connInfos) {
//		List<ClientContext> contexts = new ArrayList<>();
//		for (ConnInfo connInfo : connInfos) {
//			contexts.add(open(connInfo));
//		}
//		return contexts.toArray(new ClientContext[contexts.size()]);
//	}
	
	
//	public static ClientContext initialContext(ConnInfo connInfo) {
//		log.info("initial a new client context. connId: {}", connInfo.connId());
//		// new & save context
//		ClientContext clientContext = ClientContextHolder.newContext(connInfo);
//		return clientContext;
//	}

	/**
	 * create a client connection(without AuthSuccessListener & Scheduler)
	 *
	 * @param connInfo
	 */
	public static void connect(ConnInfo connInfo) {
		ClientContext clientContext = ClientContextHolder.newContext(connInfo);
		connect(connInfo, clientContext);
	}

	/**
	 * create a client connection with AuthSuccessListener
	 * @param connInfo
	 * @param authSuccessListeners
	 */
	public static void connect(ConnInfo connInfo, List<AuthSuccessListener> authSuccessListeners) {
		ClientContext clientContext = ClientContextHolder.newContext(connInfo);
		if (authSuccessListeners != null && authSuccessListeners.size() > 0) {
			clientContext.addAuthSuccessListeners(authSuccessListeners);
		}
		connect(connInfo, clientContext);
	}

	/**
	 * create a client connection with AuthSuccessListener & Scheduler
	 * @param connInfo
	 * @param authSuccessListeners
	 * @param schedulers
	 */
	public static void connect(ConnInfo connInfo, List<AuthSuccessListener> authSuccessListeners, List<Scheduler> schedulers) {
		ClientContext clientContext = ClientContextHolder.newContext(connInfo);
		if (authSuccessListeners != null && authSuccessListeners.size() > 0) {
			clientContext.addAuthSuccessListeners(authSuccessListeners);
		}
		if (schedulers != null && schedulers.size() > 0) {
			clientContext.addAuthSuccessSchedulers(schedulers);
		}
		connect(connInfo, clientContext);
	}

	private static void connect(ConnInfo connInfo, ClientContext clientContext) {
		ClientConnector clientConnector = ClientContextHolder.initConnector(connInfo.connId());
		clientConnector.connect(clientContext);
	}
	
//	public static ClientContext initialContextAndConnect(ConnInfo connInfo) {
//		ClientContext clientContext = initialContext(connInfo);
//		connect(connInfo, clientContext);
//		return clientContext;
//	}


	public static boolean isConnected(String connId) {
		return ConnStatus.CONNECTED == ClientContextHolder.getContext(connId).getConnStatus();
	}

	public static void disconnect(String connId) {
		ClientContextHolder.getClientConnector(connId).disconnect();
		// channel inactive will deploy and clear context/session
//		ClientContextHolder.getContext(connId).destroy();
	}

//	public static void reopen(ConnInfo connInfo) {
//		close(connInfo.connId());
//		initialContextAndConnect(connInfo);
//	}


}
