# scomu

#### 介绍
socket通信的客户端和服务端的通用库，基于Netty和ProtoBuf。

#### 软件架构

![scomu sequence diagram](https://images.gitee.com/uploads/images/2020/0722/131812_8b6303e5_7784580.jpeg "scomu-sd.JPG")

#### 软件架构说明
	








#### 安装教程

1.  在项目根路径执行:

    mvn clean install -Dmaven.test.skip=true


#### 使用说明

1.  scomu-client和scomu-server分别在(src/test/java)有对应的测试启动类ClientStarter.java和ServerStarter.java，
    
    可以直接run as -> Java Application 启动。
	
2.  demo启动方式：

    server demo 启动：java -jar /your_path/scomu-server-demo-1.0.0-SNAPSHOT.jar

    server demo访问: http://localhost:9002/swagger-ui.html
        ![Image description](https://images.gitee.com/uploads/images/2021/0614/020113_ccbc4e85_7784580.png "scomu-server-demo.png")


    client demo 启动：java -jar /your_path/scomu-client-demo-1.0.0-SNAPSHOT.jar

    client demo访问: http://localhost:9001/swagger-ui.html
        ![Image description](https://images.gitee.com/uploads/images/2021/0614/020126_147182bb_7784580.png "scomu-client-demo.png")
    

    


#### Roadmap


    1. 通过访问redis获取用户密码 （留了placehold，代码未实现）
    2. server验证srvRandAnswer通过后生成的accessToken保存Redis（未实现）
    3. 实现基于zookeeper的server集群
    4. 服务端，客户端，demo支持docker部署
    5. 


